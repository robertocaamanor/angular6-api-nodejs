import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantsComponent } from './components/restaurants/restaurants.component';
import { AddRestaurantComponent } from './components/restaurants/add-restaurant/add-restaurant.component';
import { EditRestaurantComponent } from './components/restaurants/edit-restaurant/edit-restaurant.component';

const routes: Routes = [
  {path: 'restaurants', component: RestaurantsComponent},
  {path: 'restaurants/add', component: AddRestaurantComponent},
  {path: 'restaurants/edit/:id', component: EditRestaurantComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
