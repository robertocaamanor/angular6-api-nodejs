import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RestaurantsComponent } from './components/restaurants/restaurants.component';
import { LoginComponent } from './components/login/login.component';
import { AddRestaurantComponent } from './components/restaurants/add-restaurant/add-restaurant.component';
import { EditRestaurantComponent } from './components/restaurants/edit-restaurant/edit-restaurant.component';

@NgModule({
  declarations: [
    AppComponent,
    RestaurantsComponent,
    LoginComponent,
    AddRestaurantComponent,
    EditRestaurantComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
