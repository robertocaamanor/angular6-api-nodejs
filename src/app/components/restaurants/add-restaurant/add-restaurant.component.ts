import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { ConfigService } from '../../../config.service';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.css']
})
export class AddRestaurantComponent implements OnInit {

  restaurantForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private router: Router, private configService: ConfigService) { }

  ngOnInit() {
    this.restaurantForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
    });
  }

  addRestaurant() {
    const name = {
      name: this.restaurantForm.controls.name.value,
    };
 
    this.configService.addRestaurant(name.name)
      .subscribe(res => {
        // console.log(name.name);
          let id = res['_id'];
          this.router.navigate(['/restaurants']);
        }, (err) => {
          console.log(err);
        });
  }

}
