import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import { ConfigService } from '../../../config.service';

@Component({
  selector: 'app-edit-restaurant',
  templateUrl: './edit-restaurant.component.html',
  styleUrls: ['./edit-restaurant.component.css']
})
export class EditRestaurantComponent implements OnInit {

  restaurantForm: FormGroup;
  _id:string='';
  name:string='';
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private configService: ConfigService) { }

  ngOnInit() {
    this.restaurantForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
    });
    this.loadRestaurant();
    
  }
  loadRestaurant() {
    this.configService.viewRestaurants(this.route.snapshot.params['id']).subscribe((data: any) => {
      this._id = data._id;
      this.restaurantForm.setValue({
        name: data.name
      });
    });
  }
  editRestaurant() {
    const name = {
      name: this.restaurantForm.controls.name.value,
    };
 
    this.configService.editRestaurants(this._id, name.name)
      .subscribe(res => {
        // console.log(name.name);
          let id = res['_id'];
          this.router.navigate(['/restaurants']);
        }, (err) => {
          console.log(err);
        });
  }
}
