import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css']
})

export class RestaurantsComponent implements OnInit {
  restaurants;
  id:string='';
  constructor(private configService: ConfigService, private router: Router) { }

  ngOnInit() {
    this.getAllRestaurants();
  }

  getAllRestaurants(){
    this.configService.getResturants().subscribe((data) => {
      console.log(data);
      this.restaurants = data;
    });
  }

  deleteRestaurants(id) {
    this.configService.deleteRestaurants(id).subscribe(res => {
      // console.log(name.name);
        let id = res['_id'];
        this.getAllRestaurants();
      }, (err) => {
        console.log(err);
      });
  }

}
