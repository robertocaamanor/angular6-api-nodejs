import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private http: HttpClient) { }
  url = 'http://localhost:3005/v1';

  login(email, password) {
    return this
            .http.post(`${this.url}/account/login`, { email: email, password: password });
  }

  addRestaurant(name) {
    return this
            .http.post(`${this.url}/restaurant/add`, { name: name });
  }

  getResturants() {
    return this
            .http
            .get(`${this.url}/restaurant`);
  }

  viewRestaurants(id){
    return this
            .http
            .get(`${this.url}/restaurant/` + id);
  }

  editRestaurants(id, name) {
    return this
            .http
            .put(`${this.url}/restaurant/` + id, { name: name });
  }

  deleteRestaurants(id) {
    return this
            .http
            .delete(`${this.url}/restaurant/` + id);
  }
}
